# Infrastructure as Code TP MySQL

## Que fait ce repo ?

Ce repo permet de déployer automatiquement le TP de base de données en utilisant `Ansible` et `Vagrant`.

Concrètement le repo automatise les parties suivantes du TP:

* 1 - Mise en place de l'environnement (LVM)
* 2 - Mise en place de MySQL
* 3 - Requêtes et configuration de MySQL
* 4 - Requêtes et configuration de MySQL
* 6 - Mise en place de la réplication

Je considère que la partie 5 n'a pas besoin d'être ajoutée dans le provisioning (script + à but éducatif: pas besoin de le relancer plusieurs fois, fichier `.sql`).

## Démo du provisioning

[![asciicast](https://asciinema.org/a/DN3ycqLrQH9vW0p4XOyDMITUC.svg)](https://asciinema.org/a/DN3ycqLrQH9vW0p4XOyDMITUC)

Pour jouer en local le déroulement de l'enregistrement, il faut l'outil `asciinema`.

```sh
asciinema play demo.cast
```

## Comment reproduire ce build ?

Il faut disposer d'Ansible, de Vagrant (ainsi que du provider libvirt, donc d'une machine Linux) et exécuter la commande :

```sh
vagrant up
```